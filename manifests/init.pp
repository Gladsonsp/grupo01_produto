# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include grupo01_produto
class grupo01_produto {
exec { 'refresh_produto':  
	command => "/usr/bin/systemctl restart produto",  
	refreshonly => true,
  }
exec { 'refresh_daemon':  
	command => "/usr/bin/systemctl daemon-reload",  
	refreshonly => true,
  }
group { 'java':
       ensure => 'present',
       gid    => '501',
     }
user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }
file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { '/opt/apps/produto':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { "/etc/systemd/system/produto.service":
        mode => "0644",        
	owner => 'root',       
	group => 'root',        
	source => 'puppet:///modules/grupo01_produto/produto.service',     
	notify => Exec['refresh_daemon'],
  }
 

remote_file { '/opt/apps/produto/produto.jar':
    ensure => latest,
    owner => java,
    group => java,
    source => 'http://23.96.113.162/artfactory/grupo01/produto/produto.jar',
  }
service { 'produto_service':
	name => produto,
	enable => true,
	start => true,
 }
package { 'maven':
        ensure => installed,
        name   => $maven,
 }
}

